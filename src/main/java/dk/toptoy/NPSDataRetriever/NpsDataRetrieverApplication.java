package dk.toptoy.NPSDataRetriever;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import dk.toptoy.NPSDataRetriever.job.DataRetriever;

@SpringBootApplication
public class NpsDataRetrieverApplication {

	public static void main(String[] args) {
		ApplicationContext app = SpringApplication.run(NpsDataRetrieverApplication.class, args);
		DataRetriever dataRetriever = app.getBean(DataRetriever.class);
		dataRetriever.getData();
	}
}
