package dk.toptoy.NPSDataRetriever.job;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import uk.co.lucasweb.aws.v4.signer.SigningException;
import uk.co.lucasweb.aws.v4.signer.hash.Base16;

@Component
public class DataRetriever {

	private static final Logger log = LoggerFactory.getLogger(DataRetriever.class);
	
	public void getData() {
		try {
			log.info(DigestUtils.sha256Hex("empty string"));
			String canonicalRequest = 
					//request method
					"GET" + '\n' +
					// CanonicalURI
					"/live/websites/button\n" +
					// CanonicalQueryString 
				"" + '\n' +
					//CanonicalHeaders
					"date:20180608T003735Z\n" + 
					"host:data.usabilla.com\n" +
					//SignedHeaders 
					"date;host" + '\n' +
					//HexEncode(Hash(RequestPayload))
					"d3f3165289cc4cfbf8b33efe78f90e2bd5133084ab8593f12c19f9a0cdaca597";
			log.info(canonicalRequest);
			
			String stringToSign =
					//Algorithm 
					"USBL1-HMAC-SHA256\n"+
					//RequestDate 
					"20180608T003735Z\n" +
					//CredentialScope 
					"20180608/usbl1_request\n" +
					//HashedCanonicalRequest - base 16 encoded - lowercase
					DigestUtils.sha256Hex(canonicalRequest).getBytes("UTF-8");
			log.info(stringToSign);
			
			String sign = generateSign(stringToSign);
			log.info(sign);
			
			String authHeader = "USBL1-HMAC-SHA256 Credential= {toptoyAccessID}/20180608/usbl1_request, SignedHeaders=date;host, Signature="+sign;
			CloseableHttpClient client = HttpClients.createDefault();
		    HttpGet httpGet = new HttpGet("https://data.usabilla.com/live/websites/button");
		    log.info("Accessing URL to download NPS data: {0}", httpGet.getURI());
		    httpGet.addHeader("Authorization", authHeader);
		    for(Header header : httpGet.getAllHeaders()) {
		    	log.info("Header name - " + header.getName() + ", value - " + header.getValue());
		    }
		    
		    CloseableHttpResponse response = null;
			response = client.execute(httpGet);
		    log.info(""+response.getStatusLine().getStatusCode());
		    BufferedReader rd = new BufferedReader(
		    		new InputStreamReader(response.getEntity().getContent()));
	    	StringBuffer result = new StringBuffer();
	    	String line = "";
	    	while ((line = rd.readLine()) != null) {
	    		result.append(line);
	    	}
		    client.close(); 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String generateSign(String stringToSign) throws UnsupportedEncodingException {
		byte[] kSecret = ("USBL1" + "{toptoySecret}").getBytes("UTF-8");
        byte[] kDate = hmacSha256(kSecret, "20180608");
        byte[] kSigning = hmacSha256(kDate, "usbl1_request");
        return Base16.encode(hmacSha256(kSigning, stringToSign)).toLowerCase();
	}
	
	private static byte[] hmacSha256(byte[] key, String value) {
        try {
            String algorithm = "HmacSHA256";
            Mac mac = Mac.getInstance(algorithm);
            SecretKeySpec signingKey = new SecretKeySpec(key, algorithm);
            mac.init(signingKey);
            return mac.doFinal(value.getBytes("UTF-8"));
        } catch (Exception e) {
            throw new SigningException("Error signing request", e);
        }
    }

}
